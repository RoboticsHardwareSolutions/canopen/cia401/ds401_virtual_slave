#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include "stdbool.h"
#include "ds401.h"


volatile bool quit = false;

void sigterm(int signo) {
    quit = true;
}

int main(int argc, char *argv[]) {

    if ((signal(SIGTERM, sigterm) == SIG_ERR) ||
        (signal(SIGINT, sigterm) == SIG_ERR) ||
        (signal(SIGTSTP, sigterm) == SIG_ERR)) {
        perror("Error define signal heandler\r\n");
        return errno;
    }
    ds401_init();


    while (!quit) {

        ds401_loop();

    }

    ds401_stop();
    return 0;
}


